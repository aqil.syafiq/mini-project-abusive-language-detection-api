from fastapi import FastAPI, Request
from predict import predict
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

app = FastAPI()

@app.get("/")
def check():
    return {"STATUS" : "API ONLINE"}


@app.post("/infer")
async def inference(body : Request):
    '''
    body : Json Request body, expected :
    {
        Sentences : array of string
    }
    '''
    data = await body.json()
    print(data)
    sentences = data['Sentences']
    result = {}
    for sentence in sentences:
        result[sentence] = predict(sentence)
    print(result)
    '''
    result here expect type of standard python dictionary
    '''
    return JSONResponse(content=jsonable_encoder(result))
